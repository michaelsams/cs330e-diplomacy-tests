#!/usr/bin/env python3

# -------------------------------
# projects/diplomacy/TestDiplomacy.py
# Copyright (C) 
# Glenn P. Downing
# -------------------------------

# https://docs.python.org/3.6/reference/simple_stmts.html#grammar-token-assert_stmt

# -------
# imports
# -------

from io import StringIO
from unittest import main, TestCase

from Diplomacy import army, diplomacy_read, diplomacy_solve, diplomacy_print

# -----------
# TestDiplomacy
# -----------


class TestDiplomacy (TestCase):
    # ----
    # read
    # ----

    def test_read_1(self):
        s = "A Madrid Hold\n"
        i = diplomacy_read(s)
        self.assertEqual(i, ["A", "Madrid", "Hold"])

    def test_read_2(self):
        s = "B Barcelona Move Madrid\n"
        i = diplomacy_read(s)
        self.assertEqual(i, ["B", "Barcelona", "Move", "Madrid"])

    def test_read_3(self):
        s = "D Paris Support B\n"
        i = diplomacy_read(s)
        self.assertEqual(i, ["D", "Paris", "Support", "B"])

    def test_read_4(self):
        with self.assertRaises(AssertionError):
            s = "1 Barcelona Hold\n"
            diplomacy_read(s)

    def test_read_5(self):
        with self.assertRaises(AssertionError):
            s = "AB Madrid Hold\n"
            diplomacy_read(s)

    def test_read_6(self):
        with self.assertRaises(AssertionError):
            s = "C 123 Move Madrid"
            diplomacy_read(s)

    def test_read_7(self):
        with self.assertRaises(AssertionError):
            s = "1 Barcelona Move\n"
            diplomacy_read(s)

    def test_read_8(self):
        with self.assertRaises(AssertionError):
            s = "C London Hold Madrid\n"
            diplomacy_read(s)
    
    def test_read_9(self):
        with self.assertRaises(AssertionError):
            s = "B Seoul Move 1333\n"
            diplomacy_read(s)

    def test_read_10(self):
        with self.assertRaises(AssertionError):
            s = "D Paris Support Bob\n"
            diplomacy_read(s)

    def test_read_11(self):
        with self.assertRaises(AssertionError):
            s = "D Paris Support 12\n"
            diplomacy_read(s)

    def test_read_12(self):
        with self.assertRaises(AssertionError):
            s = "Only two"
            diplomacy_read(s)

    def test_read_13(self):
        with self.assertRaises(AssertionError):
            s = ""
            diplomacy_read(s)

    # ----
    # solve 
    # ---- 

    # Changed input values from 1 to corresponding results
    
    def test_solve_1(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Support B")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A [dead]\nB Madrid\nC London\n")
        
        
    def test_solve_2(self):
        r = StringIO("A Madrid Move Barcelona\nB Barcelona Move Madrid")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A Barcelona\nB Madrid\n")
    
    
    def test_solve_3(self):
        r = StringIO("A Barcelona Move Madrid\nB Madrid Move Barcelona\nC Lisbon Move Barcelona\nD Vienna Move Barcelona\nE Berlin Support D")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A Madrid\nB [dead]\nC [dead]\nD Barcelona\nE Berlin\n")
    
    
    def test_solve_4(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Support B\nD Austin Move London")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A [dead]\nB [dead]\nC [dead]\nD [dead]\n")
    
    def test_solve_5(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Move Seoul")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A [dead]\nB [dead]\nC Seoul\n")

    def test_solve_6(self):
        r = StringIO("A Madrid Move Pluto\nB Paris Hold")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A Pluto\nB Paris\n")

    def test_solve_7(self):
         r = StringIO("A Madrid Move Barcelona\nB Barcelona Move London\nC London Move Madrid")
         w = StringIO()
         diplomacy_solve(r, w)
         self.assertEqual(w.getvalue(), "A Barcelona\nB London\nC Madrid\n")

    def test_solve_8(self):
        r = StringIO("A Seoul Hold\nB Barcelona Move Seoul\nC London Move Seoul\nD Tokyo Support B\nE Austin Support A\n")
        w = StringIO()
        diplomacy_solve(r,w)
        self.assertEqual(w.getvalue(), "A [dead]\nB [dead]\nC [dead]\nD Tokyo\nE Austin\n")

    def test_solve_9(self):
        r = StringIO("A Tokyo Hold\nB Chicago Support A\nC London Support B\nD Austin Move Chicago\nE Toronto Move Tokyo")
        w = StringIO()
        diplomacy_solve(r,w)
        self.assertEqual(w.getvalue(), "A [dead]\nB Chicago\nC London\nD [dead]\nE [dead]\n")
    
    def test_solve_10(self):
        r = StringIO("A Seoul Hold\nB Barcelona Move Seoul\nC London Move Seoul\nD Tokyo Support B")
        w = StringIO()
        diplomacy_solve(r,w)
        self.assertEqual(w.getvalue(), "A [dead]\nB Seoul\nC [dead]\nD Tokyo\n")

    def test_solve_11(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Move Madrid")
        w = StringIO()
        diplomacy_solve(r,w)
        self.assertEqual(w.getvalue(), "A [dead]\nB [dead]\nC [dead]\n")
    # -----
    # print
    # -----

    def test_print_1(self):
        w = StringIO()
        example_army = army("A", "Madrid", "Hold")
        diplomacy_print(w, [example_army])
        self.assertEqual(w.getvalue(), "A Madrid\n")

    def test_print_2(self):
        w = StringIO()
        example_army = army("B", "Seoul", "Hold")
        example_army.alive = False
        diplomacy_print(w, [example_army])
        self.assertEqual(w.getvalue(), "B [dead]\n")

    def test_print_3(self):
        w = StringIO()
        example_army = army("C", "Barcelona", "Move", "Madrid")
        example_army.city = "Madrid"
        diplomacy_print(w, [example_army])
        self.assertEqual(w.getvalue(), "C Madrid\n")

# ----
# main
# ----

if __name__ == "__main__":
    main()

""" #pragma: no cover
$ coverage run --branch TestDiplomacy.py >  TestDiplomacy.out 2>&1


$ cat TestDiplomacy.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK


$ coverage report -m                   >> TestDiplomacy.out



$ cat TestDiplomacy.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK
Name             Stmts   Miss Branch BrPart  Cover   Missing
------------------------------------------------------------
Diplomacy.py          12      0      2      0   100%
TestDiplomacy.py      32      0      0      0   100%
------------------------------------------------------------
TOTAL               44      0      2      0   100%
"""
