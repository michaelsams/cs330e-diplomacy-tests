#!/usr/bin/env python3

# -------------------------------
# projects/diplomacy/TestDiplomacy.py
# -------------------------------

# https://docs.python.org/3.6/reference/simple_stmts.html#grammar-token-assert_stmt

# -------
# imports
# -------

from io import StringIO
from unittest import main, TestCase

from Diplomacy import diplomacy_read, diplomacy_eval, diplomacy_print, diplomacy_solve

# -----------
# TestCollatz
# -----------


class TestCollatz (TestCase):
    # ----
    # read
    # ----

    def test_read1(self):
        s = "A Madrid Hold\n"
        i = diplomacy_read(s)
        self.assertEqual(i,  ['A', 'Madrid', 'Hold'])

    def test_read2(self):
        s = "B Barcelona Move Madrid\n"
        i = diplomacy_read(s)
        self.assertEqual(i,  ['B', 'Barcelona', 'Move', 'Madrid'])

    def test_read3(self):
        s = "C Paris Support B\n"
        i = diplomacy_read(s)
        self.assertEqual(i,  ['C', 'Paris', 'Support', 'B'])

    # ----
    # eval
    # ----

    def test_eval_1(self):
        v = diplomacy_eval([['A', 'Madrid', 'Hold'], ['B', 'Barcelona', 'Move', 'Madrid'], ['C', 'Paris', 'Support', 'B']])
        self.assertEqual(v, [['A', '[dead]'], ['B', 'Madrid'], ['C', 'Paris']])

    def test_eval_2(self):
        v = diplomacy_eval([['A', 'NewYork', 'Hold'], ['B', 'Boston', 'Move', 'NewYork'], ['C', 'Vancouver', 'Move', 'NewYork'], ['D', 'Perth', 'Support', 'B'], ['E', 'Dublin', 'Support', 'A']])
        self.assertEqual(v, [['A', '[dead]'], ['B', '[dead]'], ['C', '[dead]'], ['D', 'Perth'], ['E', 'Dublin']])

    def test_eval_3(self):
        v = diplomacy_eval([['A', 'Austin', 'Move', 'Paris'], ['B', 'Barcelona', 'Move', 'Paris'], ['C', 'Tokyo', 'Move', 'Barcelona'], ['D', 'Seattle', 'Move', 'Tokyo']])
        self.assertEqual(v, [['A', '[dead]'], ['B', '[dead]'], ['C', 'Barcelona'], ['D', 'Tokyo']])


    # -----
    # print
    # -----

    def test_print1(self):
        w = StringIO()
        diplomacy_print(w, 'A', '[dead]')
        self.assertEqual(w.getvalue(), "A [dead]\n")

    def test_print2(self):
        w = StringIO()
        diplomacy_print(w, 'B', 'Madrid')
        self.assertEqual(w.getvalue(), "B Madrid\n")

    def test_print3(self):
        w = StringIO()
        diplomacy_print(w, 'C', 'Paris')
        self.assertEqual(w.getvalue(), "C Paris\n")


    # -----
    # solve
    # -----

    def test_solve(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC Paris Support B\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB Madrid\nC Paris\n")

    def test_solve1(self):
        r = StringIO("A Paris Hold\nB London Support A\nC Madrid Move London\nD Tokyo Move Paris")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A [dead]\nB [dead]\nC [dead]\nD [dead]\n")

    def test_solve2(self):
        r = StringIO("A Austin Move Paris\nB Barcelona Move Paris\nC Tokyo Move Barcelona\nD Seattle Move Tokyo\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB [dead]\nC Barcelona\nD Tokyo\n")

# ----
# main
# ----


if __name__ == "__main__":
    main()

""" #pragma: no cover
$ coverage run --branch TestDiplomacy.py >  TestDiplomacy.out 2>&1


$ cat TestDiplomacy.out
.......
---------------------------------------------------------------------
Ran 7 tests in 0.000s
OK


$ coverage report -m                   >> TestDiplomacy.out



$ cat TestDiplomacy.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK
Name             Stmts   Miss Branch BrPart  Cover   Missing
------------------------------------------------------------
Diplomacy.py          12      0      2      0   100%
TestDiplomacy.py      32      0      0      0   100%
------------------------------------------------------------
TOTAL               44      0      2      0   100%
"""

