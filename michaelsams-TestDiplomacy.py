#!/usr/bin/env python3

# -------------------------------
# projects/collatz/TestCollatz.py
# Copyright (C) 
# Glenn P. Downing
# -------------------------------

# https://docs.python.org/3.6/reference/simple_stmts.html#grammar-token-assert_stmt

# -------
# imports
# -------

from io import StringIO
from unittest import main, TestCase

# from Collatz import collatz_read, collatz_eval, collatz_print, collatz_solve
from Diplomacy import army
from Diplomacy import city
from Diplomacy import diplomacy_read, diplomacy_eval, diplomacy_print, diplomacy_solve
# from Diplomacy import army


# -----------
# TestCollatz
# -----------


class TestDiplomacy(TestCase):
    # ----
    # read
    # ----

    def test_read_1(self):
        s = ["A NewYork Hold ", "B Dallas Hold ", "C Beijing Move NewYork", "D Houston Support C"]
        s2 = StringIO("A NewYork Hold\nB Dallas Hold\nC Beijing Move NewYork\nD Houston Support C\n")
        army_lst = diplomacy_read(s2)
        for i in range(len(army_lst)):
            self.assertEqual(army_lst[i].gen_str(),  s[i])

    def test_read_2(self):
        s = ["A Madrid Move London", "B Barcelona Move Madrid", "C London Hold "]
        s2 = StringIO("A Madrid Move London\nB Barcelona Move Madrid\nC London Hold\n")
        army_lst = diplomacy_read(s2)
        for i in range(len(army_lst)):
            self.assertEqual(army_lst[i].gen_str(),  s[i])

    def test_read_3(self):
        s = ["A Madrid Move Barcelona", "B Barcelona Hold ", "C London Support B", "D Austin Move London"]
        s2 = StringIO("A Madrid Move Barcelona\nB Barcelona Hold\nC London Support B\nD Austin Move London\n")
        army_lst = diplomacy_read(s2)
        for i in range(len(army_lst)):
            self.assertEqual(army_lst[i].gen_str(),  s[i])

    # ----
    # eval
    # ----

    def test_eval_1(self):
        s = [army("A", "Madrid", "Move", "Barcelona"), army("B", "Barcelona", "Hold")]
        output = [["A","[dead]"],["B", "[dead]"]]
        v = diplomacy_eval(s)
        self.assertEqual(v, output)

    def test_eval_2(self):
        s = [army("A", "Madrid", "Hold"), army("B", "Barcelona", "Hold")]
        output = [["A", "Madrid"],["B", "Barcelona"]]
        v = diplomacy_eval(s)
        self.assertEqual(v, output)

    def test_eval_3(self):
        s = [army("A", "Madrid", "Hold"), army("B", "Barcelona", "Move", "Madrid"), army("C", "Shanghai", "Support", "B")]
        output = [["A", "[dead]"],["B", "Madrid"], ["C", "Shanghai"]]
        v = diplomacy_eval(s)
        self.assertEqual(v, output)


    # # -----
    # # print
    # # -----

    def test_print_1(self):
        w = StringIO()
        r = [["A","London"],["B","Chicago"]]
        diplomacy_print(w, r)
        self.assertEqual(w.getvalue(), "A London\nB Chicago\n")

    def test_print_2(self):
        w = StringIO()
        r = [["A","[dead]"],["B","Austin"]]
        diplomacy_print(w, r)
        self.assertEqual(w.getvalue(), "A [dead]\nB Austin\n")

    def test_print_3(self):
        w = StringIO()
        r = [["A","Barcelona"],["B","[dead]"],["C", "[dead]"], ["D", "Beijing"]]
        diplomacy_print(w, r)
        self.assertEqual(w.getvalue(),"A Barcelona\nB [dead]\nC [dead]\nD Beijing\n")

    # --
    # -----
    # print
    # -----

    #def test_print(self):
    #    w = StringIO()
    #    diplomacy_print(w, [["A", "[dead]"], ["B", "Madrid"], ["C", "[dead]"], ["D", "Austin"]])
    #    self.assertEqual(w.getvalue(), "A [dead]\nB Madrid\nC [dead]\nD Austin\n")

    # # -----
    # # solve
    # # -----


    def test_solve_1(self):
        r = StringIO("A London Move Austin\nB Dublin Support C\nC Austin Hold\nD Dallas Move Dublin\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB [dead]\nC [dead]\nD [dead]\n")

    def test_solve_2(self):
        r = StringIO("A Beijing Move London\nB Dublin Move Dallas\nC London Hold\nD Dallas Move Dublin\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB Dallas\nC [dead]\nD Dublin\n")
    
    def test_solve_3(self):
        r = StringIO("A Pittsburgh Move Dallas\nB Orlando Hold\nC Dallas Hold\nD Austin Move Pittsburgh\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB Orlando\nC [dead]\nD Pittsburgh\n")


# ----
# main
# ----

if __name__ == "__main__":
    main()

# """ #pragma: no cover
# $ coverage run --branch TestDiplomacy.py >  TestCollatz.out 2>&1


# $ cat TestCollatz.out
# .......
# ----------------------------------------------------------------------
# Ran 7 tests in 0.000s
# OK


# $ coverage report -m                   >> TestCollatz.out



# $ cat TestCollatz.out
# .......
# ----------------------------------------------------------------------
# Ran 7 tests in 0.000s
# OK
# Name             Stmts   Miss Branch BrPart  Cover   Missing
# ------------------------------------------------------------
# Collatz.py          12      0      2      0   100%
# TestCollatz.py      32      0      0      0   100%
# ------------------------------------------------------------
# TOTAL               44      0      2      0   100%
# """
